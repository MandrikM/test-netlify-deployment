import React from 'react';
import ElListBig from "./ElListBig"
import Button from 'react-bootstrap/Button';
class LoadList extends React.Component {
    constructor(props) {
        let orderid =new URLSearchParams(window.location.search).get("orderid")
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            answer:false,
            orderid:orderid
        };
       // this.handleClick = this.handleClick.bind(this);
    }
    componentDidMount() {
        let orderid =new URLSearchParams(window.location.search).get("orderid")
        console.log(orderid)
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        fetch("https://mytest-4f7e5-default-rtdb.firebaseio.com/orders/test/"+orderid+".json?auth=s5oPj5UmWjvySpoiecUydMmFATg0BBO0Ujclsx7O", requestOptions)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                console.log(data);
                if (data.price&&!(data.quant&&data.term)||data.quant&&!(data.price&&data.term)||data.term&&!(data.price&&data.quant)){data.change="відбулася зміна"}else{data.change="відбулися зміни"

                }
                if(data.change=="відбулася зміна"){
                    data.str=data.quant||data.price||data.term;
                }
                this.setState({
                    isLoaded: true,
                    items:data,
                    orderid:data.id
                });

            })
            .catch(error => {
                this.setState({
                    isLoaded: true,
                    error
                });
            });
    }

    handleClick = () => {
        console.log('значение this:', this);
        var myHeaders = new Headers();
        myHeaders.append("mode", "no-cors");
        myHeaders.append("X-Requested-With", "XMLHttpRequest");
        myHeaders.append("Content-Type", "application/json");
        let orderid =new URLSearchParams(window.location.search).get("orderid")
        var raw = JSON.stringify({"orderId":orderid,"reply":"approve"});

        var requestOptions = {
            mode:'no-cors',
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://www.corezoid.com/api/1/json/public/747928/94e2cfe6403da838ac6a14c161563a187745060e", requestOptions)
            .then(response => {
                this.setState({ answer : true})
                console.log(this.state)
            })
            .catch(error => console.log('error', error));
    }
    handleClickErr = () => {
        var myHeaders = new Headers();
        myHeaders.append("mode", "no-cors");
        myHeaders.append("X-Requested-With", "XMLHttpRequest");
        myHeaders.append("Content-Type", "application/json");
        let orderid =new URLSearchParams(window.location.search).get("orderid")
        var raw = JSON.stringify({"orderId":orderid,"reply":"cancel"});

        var requestOptions = {
            mode:'no-cors',
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://www.corezoid.com/api/1/json/public/747928/94e2cfe6403da838ac6a14c161563a187745060e", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
        console.log('значение thisErr:', this);
        window.open("https://m.apteka911.ua/for_buyers/kak-sdelat-zakaz", "_self")
    }
    render() {
        const { error, isLoaded, items, answer,orderid } = this.state;
        if(!answer){
            if (error) {
                return (
                    <div>
                        <div> У Вашому замовленні {orderid} відбулися наступні зміни.</div>
                        <div>Пропонуємо Вам:</div>
                    <div>Ошибка: {error.message}</div>
                    </div>
                );
            } else if (!isLoaded) {
                return (
                    <div>

                        <div> У вашому замовленні {orderid} відбулися наступні зміни:</div>
                        <div>Пропонуємо Вам:</div>
                        <div>Завантаження...</div>
                    </div>);
            } else {
                return (
                    <div>

<aside>
    <div>Шановний клієнте!</div>
                        <div> У вашому замовленні {orderid} {items.change} {items.str}.</div>
                        <div>Пропонуємо вам:</div>
                        <ElListBig prob ={items.prob}/>
</aside>
                        <div className="test">
                            <Button onClick={this.handleClick} variant="flatt" size="xxl">Підтвердити замовлення</Button>
                            <Button onClick={this.handleClickErr}  variant="flattt" size="xxl">Зробити нове замовлення</Button>
                        </div>
                    </div>
                );
            }
        }else{
            return (<aside>
                <div>
                Дякуємо за ваш час та замовлення!
                </div>
                <div> </div>
                <div>Чекаємо за адресою: {items.address}.</div>

                {items.img ? <div> <img src={items.img} alt="pharm" /></div> : null}
            </aside>);
        }

    }
}
export default LoadList;
